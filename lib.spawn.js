var spawn = Game.spawns['Spawn1'];
var room = spawn.room;

var spawnLib = {
  creepsByRole: function(role) {
    return _.filter(Game.creeps, (creep) => creep.memory.role == role);
  },

  builders: function() {
    return spawnLib.creepsByRole('builder');
  },

  harvesters: function() {
    return spawnLib.creepsByRole('harvester');
  },

  upgraders: function() {
    return spawnLib.creepsByRole('upgrader');
  },

  canSpawnDrone: function() {
    var hasEnergy = (room.energyAvailable >= 200);
    var notSpawning = (spawn.spawning == null);
    return hasEnergy && notSpawning;
  },

  spawnDrone: function(role) {
    name = role + Game.time;
    console.log('Spawning new ' + role + ': ' + name);
    spawn.spawnCreep([WORK, CARRY, MOVE], name, { memory: {role: role}});
  },

  spawnBuilder: function() {
    spawnLib.spawnDrone('builder');
  },

  spawnHarvester: function() {
    spawnLib.spawnDrone('harvester');
  },

  spawnUpgrader: function() {
    spawnLib.spawnDrone('upgrader');
  },

  harvestersNeeded: function() {
    var live_harvesters = spawnLib.harvesters().length;
    return 2 - live_harvesters;
  },

  upgradersNeeded: function() {
    var live_upgraders = spawnLib.upgraders().length;
    var total_needed = room.controller.level * 2 - 1;
    return total_needed - live_upgraders;
  },

  buildersNeeded: function() {
    var live_builders = spawnLib.builders().length;
    var construction_sites = room.find(FIND_CONSTRUCTION_SITES);
    if(construction_sites.length > 0) {
      return 2 - live_builders;
    }
    else {
      return 0 - live_builders;
    }
  },

  autoSpawn: function() {
    spawnLib.reassignDrones();
    spawnLib.spawnHarvesterIfNeeded() ||
    spawnLib.spawnUpgraderIfNeeded() ||
    spawnLib.spawnBuilderIfNeeded();
  },

  reassignDrones: function() {
    var creepsToReassign = [];

    var excessBuilders = 0 - spawnLib.buildersNeeded();
    var excessUpgraders = 0 - spawnLib.buildersNeeded();
    var excessHarvesters = 0 - spawnLib.buildersNeeded();

    // mark excess builders
    for(var i = 0; i < excessBuilders; i++) {
      creepsToReassign.push(spawnLib.builders()[i]);
    }

    // mark excess upgraders
    for(var i = 0; i < excessUpgraders; i++) {
      creepsToReassign.push(spawnLib.upgraders()[i]);
    }

    // mark excess harvesters
    for(var i = 0; i < excessHarvesters; i++) {
      creepsToReassign.push(spawnLib.harvesters()[i]);
    }

    while(spawnLib.harvestersNeeded() > 0 && creepsToReassign.length > 0) {
      var creep = creepsToReassign.shift();
      console.log("Reassigning creep " + creep.name + " to harvester role.");
      creep.memory.role = 'harvester';
    }

    while(spawnLib.upgradersNeeded() > 0 && creepsToReassign.length > 0) {
      var creep = creepsToReassign.shift();
      console.log("Reassigning creep " + creep.name + " to upgrader role.");
      creep.memory.role = 'upgrader';
    }

    while(spawnLib.buildersNeeded() > 0 && creepsToReassign.length > 0) {
      var creep = creepsToReassign.shift();
      console.log("Reassigning creep " + creep.name + " to builder role.");
      creep.memory.role = 'builder';
    }

    while(creepsToReassign.length > 0) {
      var creep = creepsToReassign.pop();
      console.log("Creep " + creep.name + " has outlived its usefulness, terminating.");
      creep.suicide();
    }

  },

  spawnHarvesterIfNeeded: function() {
    if(!spawnLib.canSpawnDrone()) {
      return;
    }

    if(spawnLib.harvestersNeeded() > 0) {
      spawnLib.spawnHarvester();
      return true;
    }
  },

  spawnUpgraderIfNeeded: function() {
    if(!spawnLib.canSpawnDrone()) {
      return;
    }

    if(spawnLib.upgradersNeeded() > 0) {
      spawnLib.spawnUpgrader();
      return true;
    }
  },

  spawnBuilderIfNeeded: function() {
    if(!spawnLib.canSpawnDrone()) {
      return;
    }

    if(spawnLib.buildersNeeded() > 0) {
      spawnLib.spawnBuilder();
      return true;
    }
  }
}

module.exports = spawnLib;
